package com.incasafit.codefit.dao.model.log;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "log_attribute_key")
@Data
public class LogAttributeKey {

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String id;

    @Column(name = "attr_type", nullable = false)
    private String type;

    @Column(name = "attr_name", nullable = false)
    private String name;

    @Column(name = "attr_path", nullable = false)
    private String jsonPath;
}
