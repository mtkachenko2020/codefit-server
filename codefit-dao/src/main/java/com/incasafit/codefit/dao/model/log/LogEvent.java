package com.incasafit.codefit.dao.model.log;

import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "log_event")
@Data
public class LogEvent {

    @Id // Manually set
    private String id;

    @Column(name = "log_type")
    private String type;

    @Column(name = "log_user_id")
    private String userId;

    @Column(name = "log_ride_id")
    private String rideId;

    @Column(name = "log_region_id")
    private String regionId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "log_payload_id")
    private LogPayload payload;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "log_error_id")
    private LogPayload error;

    @Column(name = "log_time")
    private DateTime time = DateTime.now();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "attr_event_id", referencedColumnName = "ID")
    private Set<LogAttribute> attributes;
}
