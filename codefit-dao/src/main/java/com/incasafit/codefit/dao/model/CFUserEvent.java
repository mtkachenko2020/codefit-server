package com.incasafit.codefit.dao.model;

import com.vividsolutions.jts.geom.Point;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cf_user_event")
@Data
public class CFUserEvent {

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String userEventId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "login_date", nullable = false)
    private DateTime loginDate;

    @Column(name = "location", columnDefinition = "Geometry")
    private Point location;

    @Column(name = "region_id", nullable = false)
    private String regionId;

    @Column(name = "user_type", nullable = false)
    private String userType;
}
