package com.incasafit.codefit.dao.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fd_bank_account")
@Data
public class CFBankAccount extends AbstractBaseEntity {

    private static final long serialVersionUID = 3867953818288393756L;

    @Column(name = "bank_account_stripe_id", nullable = false)
    private String stripeBankAccountId;

    @Column(name = "bank_account_account_holder_name")
    private String accountHolderName;

    @Column(name = "bank_account_bank_name")
    private String bankName;

    @Column(name = "bank_account_country")
    private String country;

    @Column(name = "bank_account_currency")
    private String currency;

    @Column(name = "bank_account_last4")
    private String last4;

    @Column(name = "bank_account_routing_number")
    private String routingNumber;

    @OneToOne
    @JoinColumn(name = "stripe_account_id", nullable = false)
    private CFStripeAccount stripeAccount;
}
