package com.incasafit.codefit.dao.model;

import com.incasafit.codefit.common.app.AppName;
import com.incasafit.codefit.common.app.AppOS;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * Contains FCM client tokens
 */
@Entity
@Table(name = "cf_fcm_token")
public class CFClientToken extends AbstractBaseEntity {
    @Column(name = "token", nullable = false, unique = true)
    private String token;

    @Column(name = "token_os", nullable = false)
    @Enumerated(EnumType.STRING)
    private AppOS appOS;

    @Column(name = "token_app_name", nullable = false)
    @Enumerated(EnumType.STRING)
    private AppName appName;

    @Column(name = "token_user_id", nullable = false)
    private String userId;

}
