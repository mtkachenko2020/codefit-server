package com.incasafit.codefit.dao.model;

import com.incasafit.codefit.common.app.AppName;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cf_refresh_token")
public class CFRefreshToken {

    @Id
    @Column(name = "rt_token")
    private String token;

    @ManyToOne
    @JoinColumn(name = "rt_user_id", nullable = false)
    private CFUser user;

    @Column(name = "rt_device_name", nullable = false)
    private String deviceName;

    @Column(name = "rt_app_name", nullable = false)
    @Enumerated(EnumType.STRING)
    private AppName appName;

    @Column(name = "rt_created", nullable = false)
    private DateTime created = DateTime.now();

}
