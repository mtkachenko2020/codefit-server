package com.incasafit.codefit.dao.model;

import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cf_phone_code")
@Data
public class CFPhoneCode {

    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "code", nullable = false)
    private int code;

    @Column(name = "phone_code_expires", nullable = false)
    private DateTime expires;

    @Column(name = "verification_attempts", nullable = false)
    private int attempts;

    @Column(name = "verification_first_attempt", nullable = false)
    private DateTime firstAttempt;

    public void setFirstAttempt(DateTime firstAttempt) {
        this.firstAttempt = firstAttempt;
        this.attempts = 1;
    }
}
