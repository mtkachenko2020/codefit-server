package com.incasafit.codefit.dao.model.log;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "log_payload")
@Data
public class LogPayload {

    public static final int MAX_LENGTH = 20_000;

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String id;

    @Column(name = "LOG_CONTENT", length = MAX_LENGTH)
    private String content;

}
