package com.incasafit.codefit.dao.model;

import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * User entity Common for all the roles
 */
@Entity
@Table(name = "CF_USER")
@Data
public class CFUser extends AbstractBaseEntity {

    @Column(name = "user_email", unique = true, nullable = false)
    private String email;

    @Column(name = "user_first_name")
    private String firstName;

    @Column(name = "user_last_name")
    private String lastName;

    @Column(name = "user_birthday")
    private DateTime birthday;

    @Column(name = "user_phone")
    private String phone;

    @Column(name = "user_rating")
    private Float rating;

    @Column(name = "user_stripe_customer_id")
    private String stripeCustomerId;

    @Column(name = "user_phone_verified", nullable = false)
    private boolean phoneVerified;

    @Column(name = "user_photo_url")
    private String photoUrl;

    @Column(name = "user_avatar_url")
    private String avatarUrl;

    @Column(name = "user_facebook_id")
    private String facebookId;

    @Column(name = "user_google_id")
    private String googleId;

    @Column(name = "user_password")
    private String password;

    @Column(name = "user_email_verified", nullable = false)
    private boolean emailVerified;

    private boolean passwordPlain;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CFCreditCard> creditCards = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "cf_user_permission",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "perm_code", referencedColumnName = "perm_code"))
    private Set<CFPermission> permissions = new HashSet<>();


    @ManyToMany
    @JoinTable(name = "FD_USER_ROLE",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_NAME", referencedColumnName = "ROLE_NAME"))
    private Set<CFRole> userRoles = new HashSet<>();

}
