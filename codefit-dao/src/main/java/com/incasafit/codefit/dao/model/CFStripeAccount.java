package com.incasafit.codefit.dao.model;

import lombok.Data;
import org.joda.time.DateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity to hold info regarding Managed Stripe Accounts. Even if we
 * might have part of this info already from social networks, we need to
 * re-collect it to make sure we get proper info to not cause potential
 * compliance issues down the line.
 */
@Entity
@Table(name = "cf_stripe_account")
@Data
public class CFStripeAccount extends AbstractBaseEntity {

    // Stripe TOS to be included in a section of our own TOS.
    @Column(name = "stripe_account_tos_acceptance_date", nullable = false)
    private DateTime stripeTosAcceptance;

    @Column(name = "stripe_account_tos_acceptance_ip", nullable = false)
    private String ip;

    // Apparently required for U.S.
    @Column(name = "stripe_account_ssn_last4")
    private String ssnLast4;

    @Column(name = "stripe_account_personal_number")
    private String personalIdNumber; // Not happy about storing this one..

    @Column(name = "stripe_account_country", nullable = false)
    private String country;

    @Column(name = "stripe_account_state")
    private String state;

    @Column(name = "stripe_account_city", nullable = false)
    private String city;

    @Column(name = "stripe_account_line1", nullable = false)
    private String line1;

    @Column(name = "stripe_account_line2")
    private String line2;

    @Column(name = "stripe_account_postal", nullable = false)
    private String postalCode;

    @Column(name = "stripe_account_stripe_id", nullable = false)
    private String stripeAccountId;

    @Column(name = "stripe_account_dob_month", nullable = false)
    private Integer dobMonth;

    @Column(name = "stripe_account_dob_year", nullable = false)
    private Integer dobYear;

    @Column(name = "stripe_account_dob_day", nullable = false)
    private Integer dobDay;

    @Column(name = "stripe_account_pub_key")
    private String pubKey;

    @Column(name = "stripe_account_secret_key")
    private String secretKey;

    @OneToOne
    @JoinColumn(name = "cf_user_id", nullable = false)
    private CFUser user;

    @OneToOne(mappedBy = "stripeAccount", optional = true, cascade = CascadeType.ALL, orphanRemoval = true)
    private CFBankAccount bankAccount;
}
