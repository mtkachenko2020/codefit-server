package com.incasafit.codefit.dao.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
@Data
public abstract class AbstractBaseEntity implements Serializable {
    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String id;

    @Column(name = "be_created")
    protected DateTime created = DateTime.now();

    @Column(name = "be_updated")
    protected DateTime updated;

    @Column(name = "be_disabled", nullable = false)
    protected boolean disabled;
}
