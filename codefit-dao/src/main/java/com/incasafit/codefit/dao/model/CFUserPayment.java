package com.incasafit.codefit.dao.model;

import com.incasafit.codefit.common.model.PaymentStatus;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "cf_user_payment")
public class CFUserPayment {

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String id;

    @ManyToOne
    @JoinColumn(name = "up_user_id", nullable = false)
    private CFUser user;

    @Column(name = "up_client_pay", nullable = false)
    private Integer clientPay;

    @Column(name = "up_tip")
    private Integer tip;

    @Column(name = "up_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private PaymentStatus status = PaymentStatus.RESERVED;

    @Column(name = "up_reserved_charge_id", nullable = false)
    private String reservedChargeId;

    @Column(name = "up_reserved_amount", nullable = false)
    private Integer reservedAmount;

    @Column(name = "UP_CREATED", nullable = false)
    private DateTime created = DateTime.now();

    @Column(name = "up_updated")
    private DateTime updated;

    private Integer amountCharged;

}
