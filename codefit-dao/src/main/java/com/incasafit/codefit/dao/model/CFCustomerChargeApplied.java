package com.incasafit.codefit.dao.model;

import com.incasafit.codefit.common.model.StripeChargeStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "fd_customer_charge_applied")
@Data
public class CFCustomerChargeApplied extends AbstractBaseEntity {
    private String userId;

    private String driverId;

    private String rideId;

    @Enumerated(EnumType.STRING)
    @Column
    private StripeChargeStatus status;

    private String stripeChargeId;

    private Long amount;

    private String currency;

    private String customer;

    private String description;

    private String destination;

    private String dispute;

    private String application;

    private String applicationFee;

    private String authorizationCode;

    private boolean captured;

    private String failureCode;

    private String failureMessage;

    private String invoice;

    private boolean paid;

    private String receiptEmail;

    private String receiptNumber;

    private String receiptUrl;

    private boolean refunded;
}
