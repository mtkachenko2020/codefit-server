package com.incasafit.codefit.dao.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "cf_role")
@Data
public class CFRole {

    @Id // marking this as ID automatically makes this unique, non-null and
    // indexed
    @Column(name = "role_name")
    private String roleName;

    @Column(name = "role_description")
    private String description;

    @ManyToMany
    @JoinTable(name = "fd_role_permission",
            joinColumns = @JoinColumn(name = "role_name", referencedColumnName = "role_name"),
            inverseJoinColumns = @JoinColumn(name = "perm_code", referencedColumnName = "perm_code"))
    private Set<CFPermission> permissions = new HashSet<>();

}
