package com.incasafit.codefit.dao.model.log;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "log_attribute")
public class LogAttribute {

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid2")
    @GeneratedValue(generator = "uuid-gen")
    private String id;

    @Column(name = "attr_event_id")
    private String eventId;

    @ManyToOne
    @JoinColumn(name = "attr_key_id", nullable = false)
    private LogAttributeKey key;

    @Column(name = "attr_value")
    private String value;

}
