package com.incasafit.codefit.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Permission entity
 * For regional admins code consists of two parts
 * 'MREQ_<REG_ID>
 */
@Entity
@Table(name = "cf_permission")
public class CFPermission {

    @Id // marking this as ID automatically makes this unique, non-null and
    // indexed
    @Column(name = "perm_code")
    private String code;

    @Column(name = "perm_description")
    private String description;
}
