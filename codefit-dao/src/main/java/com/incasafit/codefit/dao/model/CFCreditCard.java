package com.incasafit.codefit.dao.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity to hold tokenized credit card information
 */
@Entity
@Table(name = "cf_credit_card")
@Data
public class CFCreditCard extends AbstractBaseEntity {

    @Column(name = "credit_card_country")
    private String country; // US

    @Column(name = "credit_card_last4")
    private String last4; // 1234

    @Column(name = "credit_card_type")
    private String type; // Visa

    @Column(name = "credit_card_exp_month")
    private Integer expMonth; // 6

    @Column(name = "credit_card_exp_year")
    private Integer expYear; // 2018

    @Column(name = "credit_card_stripe_id")
    private String stripeCardId;

    @Column(name = "credit_card_default")
    private Boolean defaultCard;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private CFUser user;
}
