package com.incasafit.codefit.dao.model.log;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public class LogEventType {

    public static final String ACCEPT_GROUP_RIDE = "ACCEPT_GROUP_RIDE";
    public static final String ACCEPT_REQUEST = "ACCEPT_REQUEST";
    public static final String ADD_CREDIT_CARD = "ADD_CREDIT_CARD";
    public static final String APPROVE_DRIVER = "APPROVE_DRIVER";
    public static final String CANCEL_DRIVER_APPROVAL = "CANCEL_DRIVER_APPROVAL";
    public static final String CANCEL_PICKUP_CLIENT = "CANCEL_PICKUP_CLIENT";
    public static final String CANCEL_PICKUP_DRIVER = "CANCEL_PICKUP_DRIVER";
    public static final String SYSTEM_CANCEL_PICKUP = "SYSTEM_CANCEL_PICKUP";
    public static final String CANCEL_REQUEST = "CANCEL_REQUEST";
    public static final String CAR_PHOTO_UPLOAD = "CAR_PHOTO_UPLOAD";
    public static final String CAR_UPDATE = "CAR_UPDATE";
    public static final String CHANGE_USER_STATUS = "CHANGE_USER_STATUS";
    public static final String CHARGE_CUSTOMER = "CHARGE_CUSTOMER";
    public static final String CREATE_BANK_ACCOUNT = "CREATE_BANK_ACCOUNT";
    public static final String CREATE_DRIVER = "CREATE_DRIVER";
    public static final String CREATE_STRIPE_ACCOUNT = "CREATE_STRIPE_ACCOUNT";
    public static final String DECLINE_DRIVER = "DECLINE_DRIVER";
    public static final String DECLINE_GROUP_RIDE = "DECLINE_GROUP_RIDE";
    public static final String DECLINE_REQUEST = "DECLINE_REQUEST";
    public static final String DELETE_BANK_ACCOUNT = "DELETE_BANK_ACCOUNT";
    public static final String DOC_UPDATE = "DOC_UPDATE";
    public static final String DOC_UPLOAD = "DOC_UPLOAD";
    public static final String PHOTO_UPLOAD = "PHOTO_UPLOAD";
    public static final String RATE_USER = "RATE_USER";
    public static final String SAVE_FCM_TOKEN = "SAVE_FCM_TOKEN";
    public static final String SEND_PHONE_CODE = "SEND_PHONE_CODE";
    public static final String UPDATE_STRIPE_ACCOUNT = "UPDATE_STRIPE_ACCOUNT";
    public static final String UPDATE_USER = "UPDATE_USER";
    public static final String VERIFY_PHONE = "VERIFY_PHONE";
    public static final String OTHER = "OTHER";

    public static final Set<String> VALUES = newHashSet(ACCEPT_GROUP_RIDE, ACCEPT_REQUEST, ADD_CREDIT_CARD, APPROVE_DRIVER,
            CANCEL_DRIVER_APPROVAL, CANCEL_PICKUP_CLIENT, CANCEL_PICKUP_DRIVER, SYSTEM_CANCEL_PICKUP, CANCEL_REQUEST, CAR_PHOTO_UPLOAD,
            CAR_UPDATE, CHANGE_USER_STATUS, CHARGE_CUSTOMER, CREATE_BANK_ACCOUNT, CREATE_DRIVER, CREATE_STRIPE_ACCOUNT, DECLINE_DRIVER,
            DECLINE_GROUP_RIDE, DECLINE_REQUEST, DELETE_BANK_ACCOUNT, DOC_UPDATE, DOC_UPLOAD, PHOTO_UPLOAD, RATE_USER,
            SAVE_FCM_TOKEN, SEND_PHONE_CODE, UPDATE_STRIPE_ACCOUNT,
            UPDATE_USER, VERIFY_PHONE, OTHER);
}
