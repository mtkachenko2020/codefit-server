package com.incasafit.codefit.service;

import com.incasafit.codefit.auth.service.impl.model.AuthResponse;
import com.incasafit.codefit.auth.service.impl.model.LogoutAuthRequest;
import com.incasafit.codefit.auth.service.impl.model.ProviderTokenAuthRequest;
import com.incasafit.codefit.auth.service.impl.model.RefreshTokenAuthRequest;
import com.incasafit.codefit.common.info.RequestInfo;

import java.util.Map;

public interface AuthTokenService {

    Map<String, String> loginWebProvider(String userId, String userAgent);

    AuthResponse loginMobileProvider(ProviderTokenAuthRequest request, String userId, String userAgent);

    AuthResponse loginRefreshToken(RefreshTokenAuthRequest request, String userAgent);

    AuthResponse loginWithPassword(ProviderTokenAuthRequest request, String userId, String userAgent);

    AuthResponse registerWithPassword(ProviderTokenAuthRequest request, String userId, String userAgent);

    void logoutMobile(LogoutAuthRequest request, RequestInfo requestInfo);

    AuthResponse loginWithUserIdAndPassword(ProviderTokenAuthRequest request, String userId, String userAgent);
}
