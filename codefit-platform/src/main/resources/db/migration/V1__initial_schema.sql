create table cf_user
(
    id                      varchar(36)           not null
        constraint cf_user_pkey
            primary key,
    be_created              timestamp             not null,
    be_disabled             boolean               not null,
    be_updated              timestamp,
    user_birthday           timestamp,
    user_email              varchar(255)          not null
        constraint email_unique
            unique,
    user_facebook_id        varchar(255),
    user_google_id          varchar(255),
    user_photo_url          varchar(900),
    user_avatar_url         varchar(900),
    user_first_name         varchar(60),
    user_last_name          varchar(60),
    user_phone              varchar(20),
    user_phone_verified     boolean               not null,
    user_stripe_customer_id varchar(36),
    user_rating             real,
    user_password           varchar(255),
    user_email_verified     boolean default false not null,
    password_plain          boolean               not null
);

create table cf_role
(
    role_name        varchar(36) not null
        constraint cf_role_pkey
            primary key,
    role_description varchar(255)
);

create table cf_credit_card
(
    id                    varchar(36) not null
        constraint cf_credit_card_pkey
            primary key,
    be_created            timestamp   not null,
    be_disabled           boolean     not null,
    be_updated            timestamp,
    credit_card_country   varchar(50),
    credit_card_default   boolean,
    credit_card_exp_month integer,
    credit_card_exp_year  integer,
    credit_card_last4     varchar(4),
    credit_card_stripe_id varchar(36),
    credit_card_type      varchar(32),
    user_id               varchar(36) not null
        constraint cf_credit_card_user_id_fkey
            references cf_user
);

create table cf_customer_charge_applied
(
    id                 varchar(36) not null
        constraint cf_customer_charge_applied_pkey
            primary key,
    be_created         timestamp   not null,
    be_disabled        boolean     not null,
    be_updated         timestamp,
    user_id            varchar(36) not null,
    status             varchar(128),
    stripe_charge_id   varchar(128),
    amount             bigint,
    currency           varchar(128),
    customer           varchar(128),
    description        varchar(128),
    destination        varchar(128),
    dispute            varchar(128),
    application        varchar(128),
    application_fee    varchar(128),
    authorization_code varchar(128),
    captured           boolean,
    failure_code       varchar(512),
    failure_message    varchar(1024),
    invoice            varchar(128),
    paid               boolean,
    receipt_email      varchar(128),
    receipt_number     varchar(128),
    receipt_url        varchar(128),
    refunded           boolean
);

create table cf_user_event
(
    user_event_id varchar(255) not null
        constraint cf_user_event_pkey
            primary key,
    login_date    timestamp    not null,
    region_id     varchar(255) not null,
    user_id       varchar(255) not null,
    user_type     varchar(255) not null,
    location      geometry
);

create table cf_user_permission
(
    user_id   varchar(36) not null
        constraint cf_user_permission_user_id_fkey
            references cf_user,
    perm_code varchar(36) not null
        constraint cf_user_permission_perm_code_fkey
            references cf_permission,
    constraint cf_user_permission_pkey
        primary key (user_id, perm_code)
);

create table log_attribute
(
    id            varchar(36) not null
        constraint log_attribute_pkey
            primary key,
    attr_value    varchar(511),
    attr_event_id varchar(36) not null
        constraint log_attribute_attr_event_id_fkey
            references log_event,
    attr_key_id   varchar(36) not null
        constraint log_attribute_attr_key_id_fkey
            references log_attribute_key
);

create table log_attribute_key
(
    id        varchar(36)  not null
        constraint log_attribute_key_pkey
            primary key,
    attr_name varchar(36)  not null,
    attr_type varchar(36)  not null,
    attr_path varchar(255) not null,
    constraint type_name_unique
        unique (attr_name, attr_type)
);

create table log_event
(
    id             varchar(36) not null
        constraint log_event_pkey
            primary key,
    log_region_id  varchar(36),
    log_ride_id    varchar(36),
    log_time       timestamp   not null,
    log_type       varchar(36),
    log_user_id    varchar(36),
    log_error_id   varchar(36)
        constraint log_event_log_error_id_fkey
            references log_payload,
    log_payload_id varchar(36)
        constraint log_event_log_payload_id_fkey
            references log_payload
);

create table log_payload
(
    id          varchar(36) not null
        constraint log_payload_pkey
            primary key,
    log_content varchar(20000)
);

create table cf_user_payment
(
    id                    varchar(36) not null
        constraint cf_user_payment_pkey
            primary key,
    up_created            timestamp   not null,
    up_updated            timestamp,
    up_client_pay         integer     not null,
    up_tip                integer,
    up_status             varchar(12) not null,
    up_user_id            varchar(36) not null
        constraint cf_user_payment_up_user_id_fkey
            references cf_user,
    up_reserved_charge_id varchar(50) not null,
    up_reserved_amount    integer     not null,
    amount_charged        integer
);

create table cf_permission
(
    perm_code        varchar(36) not null
        constraint cf_permission_pkey
            primary key,
    perm_description varchar(255)
);

create table cf_refresh_token
(
    rt_token       varchar(100) not null
        constraint cf_refresh_token_pkey
            primary key,
    rt_created     timestamp    not null,
    rt_device_name varchar(36)  not null,
    rt_app_name    varchar(10)  not null,
    rt_user_id     varchar(36)  not null
        constraint cf_refresh_token_rt_user_id_fkey
            references cf_user
);

create table cf_phone_code
(
    user_id                    varchar(36) not null
        constraint cf_phone_code_pkey
            primary key,
    verification_attempts      integer     not null,
    code                       integer     not null,
    phone_code_expires         timestamp   not null,
    verification_first_attempt timestamp   not null,
    phone                      varchar(20) not null
);

create table cf_fcm_token
(
    id             varchar(36)  not null
        constraint cf_fcm_token_pkey
            primary key,
    be_created     timestamp    not null,
    be_disabled    boolean      not null,
    be_updated     timestamp,
    token          varchar(255) not null
        constraint cf_fcm_token_token_key
            unique,
    token_user_id  varchar(36)  not null
        constraint cf_fcm_token_token_user_id_fkey
            references cf_user,
    token_app_name varchar(10)  not null,
    token_os       varchar(10)  not null
);

create table cf_stripe_account
(
    id                                 varchar(36)  not null
        constraint cf_stripe_account_pkey
            primary key,
    be_created                         timestamp    not null,
    be_disabled                        boolean      not null,
    be_updated                         timestamp,
    stripe_account_city                varchar(50)  not null,
    stripe_account_country             varchar(50)  not null,
    stripe_account_dob_day             integer      not null,
    stripe_account_dob_month           integer      not null,
    stripe_account_dob_year            integer      not null,
    stripe_account_tos_acceptance_ip   varchar(50)  not null,
    stripe_account_line1               varchar(255) not null,
    stripe_account_line2               varchar(255),
    stripe_account_personal_number     varchar(255),
    stripe_account_postal              varchar(12)  not null,
    stripe_account_pub_key             varchar(255),
    stripe_account_secret_key          varchar(255),
    stripe_account_ssn_last4           varchar(4),
    stripe_account_state               varchar(255),
    stripe_account_stripe_id           varchar(36)  not null,
    stripe_account_tos_acceptance_date timestamp    not null,
    cf_user_id                         varchar(36)  not null
        constraint user_unique
            unique
        constraint cf_stripe_account_cf_user_id_fkey
            references cf_user
);

create table cf_bank_account
(
    id                               varchar(36) not null
        constraint cf_bank_account_pkey
            primary key,
    be_created                       timestamp   not null,
    be_disabled                      boolean     not null,
    be_updated                       timestamp,
    bank_account_account_holder_name varchar(60),
    bank_account_bank_name           varchar(255),
    bank_account_country             varchar(50),
    bank_account_currency            varchar(5),
    bank_account_last4               varchar(4),
    bank_account_routing_number      varchar(16),
    bank_account_stripe_id           varchar(36) not null,
    stripe_account_id                varchar(36) not null
        constraint cf_bank_account_stripe_account_id_fkey
            references cf_stripe_account
);