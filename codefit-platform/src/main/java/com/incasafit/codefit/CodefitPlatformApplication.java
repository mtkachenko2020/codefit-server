package com.incasafit.codefit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@PropertySources({@PropertySource("classpath:env.properties"), @PropertySource("classpath:integration.properties")})
public class CodefitPlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(CodefitPlatformApplication.class, args);
    }
}
