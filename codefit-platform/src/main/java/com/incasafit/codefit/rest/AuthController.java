package com.incasafit.codefit.rest;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "/auth", tags = {"Auth", "SignIn", "SignUp"})
@RequestMapping("/auth")
public class AuthController {
}
