package com.incasafit.codefit.config;

import com.hazelcast.config.ClasspathXmlConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.MapConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.incasafit.codefit.common.constant.HazelcastSharedConstants.USER_READY_STATE;


@Configuration
public class HazelcastConfig {
    @Bean
    public Config config() {
        return new ClasspathXmlConfig("hazelcast.xml")
                .addMapConfig(new MapConfig(USER_READY_STATE).setInMemoryFormat(InMemoryFormat.OBJECT));
    }
}