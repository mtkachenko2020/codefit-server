package com.incasafit.codefit.auth.service.impl.model;

import io.swagger.annotations.ApiModelProperty;

public class ProviderTokenAuthRequest extends AuthRequest {

    @ApiModelProperty("social provider: google/facebook")
    public String provider;
    @ApiModelProperty("userID retrieved from provider, if empty accessToken used")
    public String providerUserID;
    @ApiModelProperty("accessToken retrieved from provider")
    public String providerAccessToken;
    @ApiModelProperty("used for new user created with password")
    public String providerFirstName;
    @ApiModelProperty("used for new user created with password")
    public String providerLastName;
    @ApiModelProperty("used for password reset")
    public int providerVerificationCode;
}
