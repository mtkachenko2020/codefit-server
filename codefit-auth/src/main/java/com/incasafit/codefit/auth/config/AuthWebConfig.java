package com.incasafit.codefit.auth.config;

import com.incasafit.codefit.auth.filter.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthWebConfig {

    @Value("${auth.token.secret}")
    private String jwtSecret;

    @Bean
    public FilterRegistrationBean authenticationFilter() {
        AuthenticationFilter filter = new AuthenticationFilter();
        filter.setJwtSecret(jwtSecret);
        final FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();

        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.addUrlPatterns("/api/*", "/auth/mobile/logout");
        return filterRegistrationBean;
    }


}
