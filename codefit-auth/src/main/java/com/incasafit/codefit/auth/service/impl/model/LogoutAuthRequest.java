package com.incasafit.codefit.auth.service.impl.model;

import io.swagger.annotations.ApiModelProperty;

public class LogoutAuthRequest extends AuthRequest {

    @ApiModelProperty("Token used to send push notifications")
    public String notificationToken;
}
