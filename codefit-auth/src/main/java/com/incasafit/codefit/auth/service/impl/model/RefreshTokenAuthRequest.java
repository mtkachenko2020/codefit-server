package com.incasafit.codefit.auth.service.impl.model;


public class RefreshTokenAuthRequest extends AuthRequest {

    public String refreshToken;
}
