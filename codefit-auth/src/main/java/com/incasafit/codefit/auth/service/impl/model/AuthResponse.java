package com.incasafit.codefit.auth.service.impl.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

public class AuthResponse {

    private static final String USER_ID = "userId";
    private static final String FIRST_NAME = "firstName";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final Boolean EMAILVERIFIED = false;

    private String userId;
    private String firstName;
    private Boolean emailVerified;
    @ApiModelProperty("JWT token valid 3 hours. Used for api requests")
    private String accessToken;

    @ApiModelProperty("Token valid until logout. Used to get new accessToken")
    private String refreshToken;

    @JsonCreator
    public AuthResponse(@JsonProperty("userId") String userId, @JsonProperty("firstName")  String firstName, @JsonProperty("accessToken") String accessToken) {
        this.userId = userId;
        this.firstName = firstName;
        this.accessToken = accessToken;
    }
    
    @JsonCreator
    public AuthResponse(@JsonProperty("userId") String userId,@JsonProperty("accessToken") String accessToken) {
        this.userId = userId;
       // this.firstName = firstName;
        this.accessToken = accessToken;
    }
    
    @JsonCreator
    public AuthResponse(@JsonProperty("userId") String userId, @JsonProperty("firstName")  String firstName,@JsonProperty("emailVerified")  Boolean emailVerified, @JsonProperty("accessToken") String accessToken) {
    	this.userId = userId;
        this.emailVerified = emailVerified;
        this.firstName = firstName;
        this.accessToken = accessToken;
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put(USER_ID, this.userId);
        map.put(FIRST_NAME, this.firstName);
        map.put(ACCESS_TOKEN, this.accessToken);
        return map;
    }

    public AuthResponse withRefreshToken(String refreshToken){
        this.refreshToken = refreshToken;
        return this;
    }

    public String getUserId() {
        return userId;
    }
    public String getFirstName() {
        return firstName;
    }
    public Boolean getEmailVerified() {
		return emailVerified;
	}

	public String getAccessToken() {
        return accessToken;
    }
    public String getRefreshToken() {
        return refreshToken;
    }
}
