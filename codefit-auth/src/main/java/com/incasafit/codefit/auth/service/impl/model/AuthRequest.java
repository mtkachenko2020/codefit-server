package com.incasafit.codefit.auth.service.impl.model;

import io.swagger.annotations.ApiModelProperty;


public abstract class AuthRequest {

    @ApiModelProperty("Human defined device name. Example: Vitaliy's Samsung or Pavlo's iPhone 7")
    public String deviceName;
    @ApiModelProperty("App name. Possible values: USER, WEB")
    public String appName;
}
