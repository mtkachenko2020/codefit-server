package com.incasafit.codefit.auth.service.impl;

import com.incasafit.codefit.auth.service.AuthTokenService;
import com.incasafit.codefit.auth.service.impl.model.AuthResponse;
import com.incasafit.codefit.auth.service.impl.model.LogoutAuthRequest;
import com.incasafit.codefit.auth.service.impl.model.ProviderTokenAuthRequest;
import com.incasafit.codefit.auth.service.impl.model.RefreshTokenAuthRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.RequestInfo;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class AuthTokenServiceImpl implements AuthTokenService {


    @Override
    public Map<String, String> loginWebProvider(String userId, String userAgent) {
        return null;
    }

    @Override
    public AuthResponse loginMobileProvider(ProviderTokenAuthRequest request, String userId, String userAgent) {
        return null;
    }

    @Override
    public AuthResponse loginRefreshToken(RefreshTokenAuthRequest request, String userAgent) {
        return null;
    }

    @Override
    public AuthResponse loginWithPassword(ProviderTokenAuthRequest request, String userId, String userAgent) {
        return null;
    }

    @Override
    public AuthResponse registerWithPassword(ProviderTokenAuthRequest request, String userId, String userAgent) {
        return null;
    }

    @Override
    public void logoutMobile(LogoutAuthRequest request, RequestInfo requestInfo) {

    }

    @Override
    public AuthResponse loginWithUserIdAndPassword(ProviderTokenAuthRequest request, String userId, String userAgent) {
        return null;
    }
}
