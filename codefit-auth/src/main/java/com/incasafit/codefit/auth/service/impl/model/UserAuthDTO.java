package com.incasafit.codefit.auth.service.impl.model;

import java.util.HashSet;
import java.util.Set;

public class UserAuthDTO {

    public final String id;
    public final Set<String> permissions;

    public UserAuthDTO(String id, Set<String> permissions) {
        this.id = id;
        this.permissions = new HashSet<>();
        if (permissions != null) {
            this.permissions.addAll(permissions);
        }
    }

    public UserAuthDTO(String id) {
        this(id, null);
    }
}
