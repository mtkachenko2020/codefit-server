package com.incasafit.codefit.common.model;

/**
 * All possible stripe charge statuses
 * status
 * string
 * The status of the payment is either succeeded, pending, or failed.
 * <p>
 * https://stripe.com/docs/api/charges/object
 */
public enum StripeChargeStatus {
    succeeded, pending, failed
}
