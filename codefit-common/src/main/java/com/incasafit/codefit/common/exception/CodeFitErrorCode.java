package com.incasafit.codefit.common.exception;

public class CodeFitErrorCode {

    // 1 - 10 Service exceptions
    public static final CodeFitErrorCode MAINTENANCE = new CodeFitErrorCode(1, "maintenance period");
    // 11 - ... Client exceptions
    public static final CodeFitErrorCode BAD_APP_VERSION = new CodeFitErrorCode(11, "unsupported app version");
    public static final CodeFitErrorCode RETRY_LIMIT_EXCEEDED = new CodeFitErrorCode(12, "retry limit exceeded");
    public static final CodeFitErrorCode INVALID_NUMBER = new CodeFitErrorCode(14, "invalid phone number");
    public static final CodeFitErrorCode RESERVATION_FAILED = new CodeFitErrorCode(15, "payment reservation failed");

    public int code;
    public String description;

    private CodeFitErrorCode(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
