package com.incasafit.codefit.common.exception;

import org.springframework.http.HttpStatus;

import java.util.Optional;


public abstract class CodeFitException extends RuntimeException {

    private static final long serialVersionUID = 5642234219567498468L;

    private final HttpStatus httpStatus;
    private CodeFitErrorCode codeFitErrorCode;

    protected CodeFitException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    protected CodeFitException withErrorCode(CodeFitErrorCode codeFitErrorCode) {
        this.codeFitErrorCode = codeFitErrorCode;
        return this;
    }

    public Optional<CodeFitErrorCode> getCodeFitErrorCode() {
        return Optional.ofNullable(codeFitErrorCode);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
