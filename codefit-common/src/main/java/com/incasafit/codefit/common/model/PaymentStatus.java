package com.incasafit.codefit.common.model;

public enum PaymentStatus {
    RESERVED, READY_TO_PAY, PAID, FAILED
}