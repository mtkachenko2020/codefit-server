package com.incasafit.codefit.common.exception;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;


public class AuthenticationException extends CodeFitException {

    private static final long serialVersionUID = 2348976209L;

    public AuthenticationException(final String message) {
        super(message, UNAUTHORIZED);
    }

    @Override
    public AuthenticationException withErrorCode(CodeFitErrorCode codeFitErrorCode) {
        return (AuthenticationException) super.withErrorCode(codeFitErrorCode);
    }
}
