package com.incasafit.codefit.common.app;

public enum AppOS {
    IOS, ANDROID
}
